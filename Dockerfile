FROM python:3.11 as builder

# Make the app package accessible
COPY dist/*.whl /app/

# Don't buffer `stdout`:
ENV PYTHONUNBUFFERED=1
# Don't create `.pyc` files:
ENV PYTHONDONTWRITEBYTECODE=1
# Make sure that the venv supersedes system's python
ENV PATH="/app/venv/bin:$PATH"

# Create virtualenv for the app
RUN python -m venv --copies /app/venv

# Install the app wheel (inside the env thanks to the PATH override)
RUN pip install /app/*.whl

# Build the production image
FROM python:3.11-slim as prod

# Install curl from healthcheck
RUN apt-get update && apt-get install curl -y \
        && rm -rf /var/lib/apt/lists/*

# Create a non-root, passwordless user to run the app
RUN groupadd -g 999 apprunner && \
    useradd -r -u 999 -g apprunner apprunner

# Copy the entire virtualenv from the builder image
COPY --chown=apprunner:apprunner --from=builder /app /app

# Set the active user to be the non-root one
USER 999

# Make sure that the virtualenv supersedes system's python
ENV PATH="/app/venv/bin:$PATH"

# App-specific settings:
ENV ROOT_PATH="/"
EXPOSE 5000
CMD uvicorn myumbrella.main:app --host 0.0.0.0 --port 5000 --root-path $ROOT_PATH
